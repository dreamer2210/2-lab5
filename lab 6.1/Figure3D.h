#pragma once
#include "Shape2D.h"

class Figure3D
{
public:
	virtual double CalcArea(Figure2D* area, double h) = 0;
	virtual Figure3D* Clone() = 0;
};

class Cylindre : public Figure3D
{
private:
	double h;
	Figure2D* area;
public:
	Cylindre(Figure2D* area, double h);
	double CalcArea(Figure2D* area, double h);
	Cylindre* Clone();
};
