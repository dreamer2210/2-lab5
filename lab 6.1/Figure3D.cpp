#include "Shape3D.h"

double Cylindre::CalcArea(Figure2D* area, double h)
{
	return area->CalcSquare() * h;
}

Cylindre::Cylindre(Figure2D* area, double h)
{
	this->area = area->Clone();
	this->h = h;
}

Cylindre* Cylindre::Clone() 
{
	return new Cylindre(*this);
}
