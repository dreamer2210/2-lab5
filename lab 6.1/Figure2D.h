#pragma once

#define _USE_MATH_DEFINES

#include <math.h>

class Figure2D
{
public:
	virtual double CalcSquare() = 0;
	virtual Figure2D* Clone() = 0;
};

class Triangle : public Figure2D
{
public:
	double CalcSquare();
	Triangle(double a, double b, double c);
	Triangle* Clone();
private:
	double a;
	double b;
	double c;
};

class Circle : public Figure2D
{
public:
	double CalcSquare();
	Circle(double r);
	Circle* Clone();
private:	
	double r;
};

class Rectangle : public Figure2D
{
private:
	double a;
	double b;
public:
	Rectangle(double a, double b);
	double CalcSquare();
	Rectangle* Clone();
};

class TriangleInRectangle : public Figure2D
{
private:
	Rectangle* rect;
	Triangle* tri;
public:
	TriangleInRectangle(Rectangle* rect, Triangle* tri);
	double CalcSquare();
	TriangleInRectangle* Clone();
};