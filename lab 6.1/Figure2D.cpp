#pragma once
#include "Shape2D.h"

Triangle::Triangle(double a, double b, double c) 
{
	this->a = a;
	this->b = b;
	this->c = c;
}

Circle::Circle(double r)
{
	this->r = r;
}

 double Triangle::CalcSquare()
 {
	 double p = (a + b + c) / 2;
	 return (p - a) * (p - b);
 }

 double Circle::CalcSquare()
 {
	 return r * r * M_PI ;
 }

Rectangle::Rectangle(double a, double b) 
{
	this->a = a;
	this->b = b;
}

double Rectangle::CalcSquare() 
{
	return a * b;
}

Circle* Circle::Clone()
{
	return new Circle(*this);
}

Triangle* Triangle::Clone()
{
	return new Triangle(*this);
}

Rectangle* Rectangle::Clone() 
{
	return new Rectangle(*this);
}

TriangleInRectangle::TriangleInRectangle(Rectangle* rect, Triangle* tri)
{
	this->rect = rect->Clone();
	this->tri = tri->Clone();
}

double TriangleInRectangle::CalcSquare()
{
	return rect->CalcSquare() - tri->CalcSquare();
}

TriangleInRectangle* TriangleInRectangle::Clone() 
{
	return new TriangleInRectangle(*this);
}

