#include "Shape3D.h"
#include <iostream>
#include <math.h>

using namespace std;

int main()
{
	setlocale(LC_ALL, "RU");

	Figure2D* rectangle = new Rectangle(5, 10);
	Figure2D* triangle = new Triangle(1, 2, 3);
	Figure2D* merge = new TriangleInRectangle((Rectangle*)rectangle, (Triangle*)triangle);

	double resultRect = rectangle->CalcSquare();
	double resultTri = triangle->CalcSquare();
	double result = merge->CalcSquare();

	cout << "������� ��������������: " << resultRect << endl;
	cout << "������� ������������: " << resultTri << endl;
	cout << "������� ������� ������: " << result << endl;

	return 0;
}  